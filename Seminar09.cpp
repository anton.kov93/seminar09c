﻿#include <iostream>
#include <time.h>

int main()
{
    setlocale(LC_ALL, "Rus");
    
    const int N = 5;
    int array[N][N];

    for (int i = 0; i < N; i++) 
    {
        for (int j = 0; j < N; j++) 
        {
            array[i][j] = i + j;
        }
    }

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            std::cout<<array[i][j];
        }
        std::cout << std::endl;
    }
    
    // Получение текущей даты и времени
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
   
    // Использование дня месяца для определения индекса строки
    int rowIndex = buf.tm_mday % N;

    std::cout << "Сегодня: " << buf.tm_mday << " число" << std::endl;
    std::cout << "Остаток от деления сегодняшнего числа на " << N << " строк массива = " 
        << buf.tm_mday << " / " << N << " = " << rowIndex << std::endl;

    if (rowIndex >= 0 && rowIndex < N)
    {
        int sum = 0;
        
        for (int j = 0; j < N; ++j)
        {
            sum = sum + array[rowIndex][j];
        }

        std::cout << "Сумма строки " << rowIndex << " = " << sum << std::endl;
    }

    else
    {
        std::cout << "Некорректный индекс строки, в массиве нет такой строки." << std::endl;
    }

}

